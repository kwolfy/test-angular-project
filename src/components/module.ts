import * as angular from 'angular';

const angularModule = angular
  .module('testApp.components', []);

export default angularModule;

