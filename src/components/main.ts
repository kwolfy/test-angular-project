
import './mc-dates.css';
import mod from './module';
import * as moment from 'moment';
import {DATE_FORMAT, DATE_FORMAT_REGEX} from "../constants";

class McDatesComponent {
  dateFrom: string;
  dateTo: string;

  inputDateFrom: string;
  inputDateTo: string;

  $onInit() {
    this.dateFrom = this.inputDateFrom = moment().format(DATE_FORMAT);
    this.dateTo = this.inputDateTo = null;
  }

  changeFrom() {
    this.dateFrom = '2019-01-05';
  }

  onChangeInputFrom() {
    if(DATE_FORMAT_REGEX.test(this.inputDateFrom)) {
      this.dateFrom = this.inputDateFrom;
    } else {
      this.dateFrom = null;
    }
  }

  onChangeInputTo() {
    if(DATE_FORMAT_REGEX.test(this.inputDateTo)) {
      this.dateTo = this.inputDateTo;
    } else {
      this.dateTo = null;
    }
  }

  onChange(from, to) {
    this.inputDateFrom = from;
    this.inputDateTo = to;

    alert(`${from} - ${to}`);
  }
}

mod.component('main', {
  template: `
    <div class="main">
      <mc-dates date-from="$ctrl.dateFrom" date-to="$ctrl.dateTo" mc-change="$ctrl.onChange(from, to)"></mc-dates>
      <md-input-container>
         <label>Дата 1</label>
         <input ng-model="$ctrl.inputDateFrom" ng-change="$ctrl.onChangeInputFrom()">
      </md-input-container>
      <md-input-container>
         <label>Дата 2</label>
         <input ng-model="$ctrl.inputDateTo" ng-change="$ctrl.onChangeInputTo()">
      </md-input-container>
    </div>
  `,
  controller: McDatesComponent,
});
