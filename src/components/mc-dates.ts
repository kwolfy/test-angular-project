import './mc-dates.css';
import mod from './module';
import * as moment from 'moment';
import {DATE_FORMAT} from "../constants";

class McDatesComponent {
  localDateFrom: Date;
  localDateTo: Date;

  set dateFrom(date: string) {
    this.localDateFrom = this.toDate(date);
  }

  set dateTo(date: string) {
    this.localDateTo = this.toDate(date);
  }

  get dateTo() {
    return this.format(this.localDateTo);
  }

  get dateFrom() {
    return this.format(this.localDateFrom);
  }


  $onInit() {
    console.log('init', this);
  }

  onChanged() {
    this.mcChange({from: this.dateFrom, to: this.dateTo});
  }

  changeRange(period: 'today' | 'yesterday' | '2weeks' | 'month' | 'all') {
    let from, to;
    const pastDay = (days) => moment(Date.now() - 86400 * days * 1000);

    if (period === 'yesterday') {
      from = to = pastDay(1);
    } else if (period === 'today') {
      from = to = moment();
    } else if (period === '2weeks') {
      from = pastDay(7);
      to = moment();
    } else if (period === 'month') {
      from = pastDay(30);
      to = moment();
    } else if (period === 'all') {
      from = null;
      to = null;
    }

    this.changeDates(from ? from.format(DATE_FORMAT) : null, to ? to.format(DATE_FORMAT) : null)
  }



  private changeDates(from: string, to: string) {
    const isChanged = this.dateFrom !== from || this.dateTo !== to;
    if(isChanged) {
      this.dateFrom = from;
      this.dateTo = to;
      this.mcChange({ from, to });
    }
  }


  private toDate(date: string) {
    if (!date) {
      return null;
    }

    return moment(date, DATE_FORMAT).toDate();
  }

  private format(date: Date) {
    if (!date) {
      return null;
    }
    return moment(date).format(DATE_FORMAT)
  }

  mcChange(dates: { from: string, to: string }) {}
}


mod.component('mcDates', {
  bindings: {
    dateFrom: '=',
    dateTo: '=',
    mcChange: '&'
  },

  template: `
    <div class="mc-dates">
      <div class="mc-dates-pick"> 
          <div>с</div>
          <md-datepicker 
            ng-model="$ctrl.localDateFrom" 
            md-placeholder="Enter date" 
            ng-change="$ctrl.onChanged()"
            md-max-date="$ctrl.localDateTo"></md-datepicker>
      </div>
      <div class="mc-dates-pick">
          <div>по</div>
          <md-datepicker 
            ng-model="$ctrl.localDateTo" 
            md-placeholder="Enter date" 
            ng-change="$ctrl.onChanged()"
            md-min-date="$ctrl.localDateFrom"
            ></md-datepicker>
      </div>
      
      <div class="mc-dates-range">
        <div ng-click="$ctrl.changeRange('yesterday')">Вчера</div>
        <div ng-click="$ctrl.changeRange('today')">Сегодня</div>
        <div ng-click="$ctrl.changeRange('2weeks')">Две недели</div>
        <div ng-click="$ctrl.changeRange('month')">Месяц</div>
        <div ng-click="$ctrl.changeRange('all')">Все</div>
      </div>
      
    </div>
  `,
  controller: McDatesComponent,
});
