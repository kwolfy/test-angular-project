import * as angular from 'angular';

import 'angular-material';
import 'angular-material/angular-material.min.css';
import './components';
import moment = require("moment");

const app = angular
  .module('testApp', ['testApp.components', 'ngMaterial'])
  .config(function($mdDateLocaleProvider: any) {

    $mdDateLocaleProvider.formatDate = function(date: string) {
      const m = moment(date);
      return m.isValid() ? moment(date).format('DD.MM.YYYY') : '';
    };

    $mdDateLocaleProvider.parseDate = function(dateString) {
      const m = moment(dateString, 'DD.MM.YYYY', true);
      return m.isValid() ? m.toDate() : new Date(NaN);
    };
  });

